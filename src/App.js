import React, { PureComponent } from 'react';
import { connect } from 'react-redux'

import { fetchGridData } from "./store/actions"
import logo from './logo.svg';
import './App.css';

class App extends PureComponent {
  componentWillMount() {
    this.props.fetchGridData()
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

const mapDispatchToProps = {
  fetchGridData
}

export default connect(null, mapDispatchToProps)(App);
