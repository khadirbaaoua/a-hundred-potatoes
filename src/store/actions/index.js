import { FETCH_GRID_DATA } from "./constants"

export const fetchGridData = () => ({
  type: FETCH_GRID_DATA
})
