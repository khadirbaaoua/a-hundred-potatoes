import { call, put, takeLatest } from 'redux-saga/effects'
import { get } from 'axios'

import {
  FETCH_GRID_DATA,
  FETCH_GRID_DATA_SUCCESS,
  FETCH_GRID_DATA_FAILED,
} from '../../actions/constants'

const FETCH_GRID_DATA_URL = 'https://www.fdj.fr/apigw/rtg/rest/euromillions'

function* fetchGridData(action) {
  try {
    const data = yield call(get, FETCH_GRID_DATA_URL)
    yield put({type: FETCH_GRID_DATA_SUCCESS, data})
  } catch (error) {
    yield put({type: FETCH_GRID_DATA_FAILED, error})
  }
}

export default function* fetchGridDataWatcher() {
  yield takeLatest(FETCH_GRID_DATA, fetchGridData)
}