import { all } from 'redux-saga/effects'

import fetchGridDataWatcher from './fetchGridData'

export default function* rootSaga() {
  yield all([
    fetchGridDataWatcher()
  ])
}