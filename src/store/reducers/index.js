import { combineReducers } from 'redux'

import fetchGridDataReducer from './fetchGridData'

export default combineReducers({
  fetchGridDataReducer
})